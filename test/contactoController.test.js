const chai = require('chai');
const chaiHttp = require('chai-http');

const contactoModel = require('../models/contactoModel');
const contactoService = require('../services/contactoService');

chai.use(chaiHttp);
require('chai').should();


describe('Contacto Controller', () => {

    let contacto = null

    before(async () => {
        contacto = await contactoService.findOneByNombre('Prueba_Contacto');
        if (!contacto) { // No existe => hay que crearlo
            contacto = new contactoModel({ nombre: 'Prueba_Contacto' });
            contacto = await contactoService.save(contacto, true);
        }

        contacto2 = await contactoService.findOneByNombre('2Prueba_Contacto');
        if (!contacto2) { // No existe => hay que crearlo
            contacto2 = new contactoModel({ nombre: '2Prueba_Contacto' });
            contacto2 = await contactoService.save(contacto, true);
        }
        server = require('../index');
    })


    it('Obtener listado de contactos', (done) => {
        chai.request(server).get('/api/contactos').end( (err, res) => {
            res.should.have.status(200);
            done();
        })
    })

    it('Agregar un contacto', (done) => {
        chai.request(server).post('/api/contactos',{nombre: 'Prueba_Contacto2'}).end( (err, res) => {
            res.should.have.status(201);
            done();
        })
    })

    it('Actualizar un contacto ok', (done) => {
        chai.request(server).put('/api/contactos/' + contacto2._id, {nombre: '2Prueba_Contacto_Actualizado'}).end( (err, res) => {
            res.should.have.status(204);
            done();
        })
    })

    it('Actualizar un contacto nok (el id no existe)', (done) => {
        console.log("POR ENTRAR");
        chai.request(server).put('/api/contactos/6666666', { nombre: 'Prueba_Contacto_Actualizado2'}).end( (err, res) => {
            res.should.have.status(500);
            done();
        })
    })
    
    it('Actualizar un contacto nok (no se pasa id)', (done) => {
        chai.request(server).put('/api/contactos/', { nombre: 'Prueba_Contacto_Actualizado2'}).end( (err, res) => {
            res.should.have.status(404);
            done();
        })
    })

    it('Borrar un contacto ok', (done) => {
        chai.request(server).delete('/api/contactos/'+contacto._id).end( (err, res) => {
            res.should.have.status(204);
            done();
        })
    })

    it('Borrar un contacto nok (el id no existe)', (done) => {
        chai.request(server).delete('/api/contactos/666666').end( (err, res) => {
            res.should.have.status(500);
            done();
        })
    })    

    after(async () => {
        await contactoService.deleteMany({nombre: 'Prueba_Contacto'});
        await contactoService.deleteMany({nombre: '2Prueba_Contacto'});
        await contactoService.deleteMany({nombre: '2Prueba_Contacto_Actualizado'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto2'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto2_Actualizado'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto3'});
        server.close();
    });

})

