
const chai = require('chai');
const nock = require('nock');

const assert = require('chai').assert;
const expect = require('chai').expect;
require('chai').should();

const server = require('../index');

const weatherService = require('../services/weatherService');

describe("weatherService", () => {

    before(async () => {

    })

    it('peticion weather ok', async () => {
        const mockServer = nock('https://samples.openweathermap.org/data/2.5').get('/weather?q=blablabla&appid=64dcbe6d53676a41fcf3058bcaa09670').reply(200, 
        {
            "coord": {
              "lon": -0.13,
              "lat": 51.51
            },
            "weather": [
              {
                "id": 300,
                "main": "Drizzle",
                "description": "light intensity drizzle",
                "icon": "09d"
              }
            ],
            "base": "stations",
            "main": {
              "temp": 280.32,
              "pressure": 1012,
              "humidity": 81,
              "temp_min": 279.15,
              "temp_max": 281.15
            }
        });
        const respuesta = await weatherService.weather("blablabla");
        respuesta.should.not.be.null;
    })

    it('peticion weather no encontrada', async () => {
        try {
            const mockServer = mockServer = nock('https://samples.openweathermap.org/data/2.5').get('/weather?q=blablabla&appid=64dcbe6d53676a41fcf3058bcaa09670').reply(404, {});
            const respuesta = await weatherService.weather("blablabla");
            assert.ok(false);
        } catch(e) {
            e.should.not.be.null
        }

    })

    after(function (done) {
        server.close();
        done();
    });

})

