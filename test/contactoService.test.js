const chai = require('chai');
const nock = require('nock');

const assert = require('chai').assert;
const expect = require('chai').expect;
require('chai').should();

const server = require('../index');

const contactoModel = require('../models/contactoModel');
const contactoService = require('../services/contactoService');
var mongoose = require('mongoose');

describe("contactoService", () => {
    let contacto = null

    before(async () => {
        contacto = await contactoService.findOneByNombre('Prueba_Contacto');
        if (!contacto) { // No existe => hay que crearlo
            contacto = new contactoModel({ nombre: 'Prueba_Contacto' });
            contacto = await contactoService.save(contacto, true);
        }
    })

    it('get(id) ok', async () => {
        const respuesta = await contactoService.get(contacto._id);
        expect(respuesta.nombre).to.eql(contacto.nombre);
        expect(respuesta._id).to.eql(contacto._id);
    })

    it('get(id) nok (buscar id que no existe)', async () => {
        try {
            const respuesta = await contactoService.get("0");
            return false;
        }
        catch {
            return true;
        }
    })

    it('list(id) ok', async () => {
        const respuesta = await contactoService.get(contacto._id);
        respuesta.should.not.be.null;
    })

    it('save(instancia, esNuevo), ok donde esNuevo=true', async () => {
        let contacto2 = new contactoModel({ nombre: 'Prueba_Contacto2' });
        respuesta = await contactoService.save(contacto2, true);
        respuesta.should.not.be.null;
    })
    
    it('save(instancia, esNuevo), ok donde esNuevo=false', async () => {
        let contacto2 = await contactoService.findOneByNombre('Prueba_Contacto2');
        contacto2.nombre = 'Prueba_Contacto2_Actualizado'; 
        respuesta = await contactoService.save(contacto2, false);
        respuesta.should.not.be.null;
        respuesta = await contactoService.findOneByNombre('Prueba_Contacto2_Actualizado');
        expect(respuesta.nombre).to.eql(contacto2.nombre);
        expect(respuesta._id).to.eql(contacto2._id);
    })

    it('save(instancia, esNuevo), nok donde esNuevo=true (insertar contacto que existe)', async () => {
        try {
            respuesta = await contactoService.save(contacto, true);
            (respuesta === null).should.be.true;
        }
        catch {
            return true;
        }
    })
    
    it('save(instancia, esNuevo), ok donde esNuevo=false (actualizar contacto que no existe)', async () => {
        let contacto2 = new contactoModel({ _id: new mongoose.Types.ObjectId(), nombre: 'Prueba_Contacto3' });
        try {
            respuesta = await contactoService.save(contacto2, false);
            (respuesta === null).should.be.true;
        }
        catch {
            return true;
        }
    })

    it('delete(id) ok', async () => {
        let contacto2 = await contactoService.findOneByNombre('Prueba_Contacto2_Actualizado');
        respuesta = await contactoService.delete(contacto2._id);
        expect(respuesta.nombre).to.eql(contacto2.nombre);
        expect(respuesta._id).to.eql(contacto2._id);
    })
    
    it('delete(id) nok (eliminar id que no existe)', async () => {
        try {
            contacto2 = await contactoService.delete(0);
            return false;
        }
        catch {
            return true;
        }
    })
    
    it('findOneByNombre(nombre) ok', async () => {
        const respuesta = await contactoService.findOneByNombre(contacto.nombre);
        expect(respuesta.nombre).to.eql(contacto.nombre);
        expect(respuesta._id).to.eql(contacto._id);
    })

    it('findOneByNombre(nombre) nok (buscar nombre que no existe)', async () => {
        const respuesta = await contactoService.findOneByNombre("0");
        (respuesta === null).should.be.true;
    })


    after(async () => {
        await contactoService.deleteMany({nombre: 'Prueba_Contacto'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto2'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto2_Actualizado'});
        await contactoService.deleteMany({nombre: 'Prueba_Contacto3'});
    });

})

