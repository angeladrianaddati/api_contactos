const chai = require('chai');
const nock = require('nock');

var express = require('express');
var app = express();
var mongoose = require('mongoose');
var ContactoModel = require("./models/contactoModel");
var body_parser = require("body-parser");
var helmet = require ( 'helmet' );
var fs = require('fs');
var https = require('https');

require('dotenv').config();

// Add security package
app. use (helmet()); 



// Conexion con la base de datos MongoDB
//mongoose.connect('mongodb://'+proccess.env.db_user+':'+proccess.env.db_pass+'@'+db_host+':'+proccess.env.db_port+'/'+proccess.env.db_collection, {}).then( () => {
// Conexion con la base de datos MongoDB
mongoose.connect('mongodb://admin:Landmark1@ds133657.mlab.com:33657/node-intermedio-tp2', {}).then( () => { 
// Conexion exitosa
    console.log('Conexion exitosa con MongoDB');
}).catch( (err) => {
    console.log('No me pude conectar con MongoDB');
    console.log(err);
})

// Memcached
var session = require("express-session");
var MemcachedStore = require('connect-memcached')(session);
var expiryDate = new Date( Date.now() + 60 * 60 * 1000 ); // 1 hour

app.use(
    session({
        key: "curso_node",
        secret: 'clave-muy-Segura01',
        proxy: "true",
        resave: false,
        saveUninitialized: false,
        cookie: { 
            secure: true,
            httpOnly: true,
            domain: 'localhost',
            //path: 'foo/bar',
            expires: expiryDate
          },
        store: new MemcachedStore({
            hosts: ['localhost']
        })
    })
);


var myLogger = function(req, res, next) { // Esta funcion se va a ejecutar en cada peticion
    console.log('Paso por el logger');
    next();
}

var myErrorHandler = function(error, req, res, next) {
    console.log('Paso por el manejador de errores');
    console.log(error.message); // Mensaje de error
    res.status(error.status || 500); // Retornamos el estado del error o 500 si no tiene estado
    res.send(error.message);
}

// La informacion enviada por POST de un formulario se recibe en req.body
app.use(body_parser.urlencoded({extended: false}));
app.use(body_parser.json());


app.use(myLogger);

app.get('/', function (request, response) {
    response.render('principal'); // views/principal.handlebars
    //response.send('Bienvenido al curso de NodeJS nivel Intermedio!');
});


app.get('/prueba', function(req, res, next) {
    // Vamos a generar un error
    next(new Error('Este error es de prueba y forzado'));
    //res.send('Respuesta de /prueba');
});

app.get('/contador', function (req, res) {
    if (!req.session.contador) {
        req.session.contador = 0;
    }
    req.session.contador++;
    res.send(`La cantidad de veces que llamaste a esta página es ${req.session.contador}`);
});

// Comienzo de la aplicacion

app.use('/api/contactos', require('./controllers/contactoAPIController')); // API

app.use(myErrorHandler);

https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
}, app).listen(process.env.PORT || 3001, function(){
    console.log('Iniciando la aplicación en https://localhost:3001 <- copia esta URL en tu navegador');
});


/*const server = app.listen(process.env || 3000, function () {
    console.log('Iniciando la aplicación en http://localhost:3000 <- copia esta URL en tu navegador');
});*/


module.exports = server;
