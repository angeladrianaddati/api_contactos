var http = require("axios");

module.exports.weather = function (cityName) {
    var baseUrl = "https://samples.openweathermap.org/data/2.5"
    var requestUrl =  "/weather";
    var appId = "64dcbe6d53676a41fcf3058bcaa09670";
    return new Promise((resolve, reject) => {
        http
            .get(
                baseUrl + requestUrl + "?q=" + cityName + "&appid=" + appId
            )
            .then(response => {
                data = { 'coord': response.data.coord, 'weather': { 'temp': response.data.main.temp, 'main': response.data.weather[0].main,  'description': response.data.weather[0].description, 'icon': response.data.weather[0].icon} };
                resolve(data);
            })
            .catch(error => {
                reject(error);
            });
    });
};
