var ContactoModel = require("../models/contactoModel");
var http = require("axios");
var mongoose = require('mongoose');
const weatherService = require('../services/weatherService');
//ObjectId = mongoose.Types.ObjectId.isValid;


module.exports.get = function (id) {
    return new Promise((resolve, reject) => {
        if (mongoose.Types.ObjectId.isValid(id)) {
                // Yes, it's a valid ObjectId, proceed with `findById` call.
            ContactoModel.findById(id, '-__v', (err, respuesta) => {
                if (err) {
                    reject(err);
                }
                else {
                    var cityName = respuesta.ciudad;
                    weatherService.weather(cityName).then(weather => {
                        let respuesta2 = {...respuesta.toJSON(), temperatura:weather.weather.temp} ;
                        resolve(respuesta2);
                    }).catch(err => {
                        resolve(respuesta);
                    })
                }
            });
        }
        else {
            reject(new Error("id not valid"));
        }
    });
};

module.exports.list = function () {
    return new Promise((resolve, reject) => {
        ContactoModel.find({ }, '-__v', (err, listado) => {
            if (err) {
                reject(err);
            }
            resolve(listado);
        });
    });
};

module.exports.save = function (instancia, esNuevo) {
    return new Promise((resolve, reject) => {
        if (!esNuevo) {
            if (mongoose.Types.ObjectId.isValid(instancia._id)) {
                // Si tiene ID es una actualizacion (cuando es alta no tiene ID)
                ContactoModel.findByIdAndUpdate(
                    instancia._id,
                    instancia,
                    (err, data) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(data);
                        }
                    }
                );
            }
            else {
                reject(new Error("id not valid"));
            }
        } else {
            // Es alta
            instancia.save((err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        }
    });
};

module.exports.delete = function (id) {
    return new Promise((resolve, reject) => {
        if (mongoose.Types.ObjectId.isValid(id)) {
            ContactoModel.findByIdAndRemove(id, (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        }
        else {
            reject(new Error("id not valid"));
        }
    });
};

module.exports.deleteMany = function (filter) {
    return new Promise((resolve, reject) => {
            ContactoModel.deleteMany(filter, (err, data) => {
                if (err) {
                    reject(err);
                }
                resolve(data);
            });
    });
};

module.exports.findOneByNombre = function (nombre) {
    return new Promise((resolve, reject) => {
        ContactoModel.findOne({ nombre: nombre }, (err, respuesta) => {
            if (err) {
                reject(err);
            }
            resolve(respuesta);
        })
    })
}


/*
module.exports.lyrics = function (artistaId, cancionId) {
    return new Promise((resolve, reject) => {
        ArtistaModel.findById(artistaId, (err, artista) => {
            if (err) {
                throw new Error("No se encontro el artista");
            }
            if (!artista) {
                throw new Error("No se encontro el artista");
            }
            CancionModel.findById(cancionId, (err, cancion) => {
                if (err) {
                    next(new Error("No se encontro la cancion"));
                }
                if (!cancion) {
                    next(new Error("No se encontro la cancion"));
                }
                var nombreArtista = artista.nombre;
                var nombreCancion = cancion.nombre;
                http
                    .get(
                        "https://api.lyrics.ovh/v1/" + nombreArtista + "/" + nombreCancion
                    )
                    .then(respuesta => {
                        resolve(respuesta.data.lyrics);
                    })
                    .catch(error => {
                        reject(error);
                    });
            });
        });
    });
};*/
