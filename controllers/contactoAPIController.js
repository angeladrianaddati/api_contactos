var app = require("express").Router();
var ContactoModel = require("../models/contactoModel");
var service = require("../services/contactoService");


/**
 * API de Contactos
 *  GET /api/contactos -> listado de todos los contactos
 *  GET /api/contactos/:id -> Obtener el contacto cuyo ID es pasado como parametro
 *  POST /api/contactos -> Agregar un contacto
 *  PUT /api/contactos/:id -> Actualizar los datos del contacto cuyo ID es pasado como parametro
 *  DELETE /api/contactos/:id -> Borrar el contacto cuyo ID es pasado como parametros
 *
 */

// Listado de contactos
app.get("/", function(req, res, next) {
  service
    .list()
    .then(listado => {
      res.send({
        "status": "ok",
        "items": listado
      });
    })
    .catch(err => {
      next(new Error("No se pudieron obtener los contactos"));
    });
});

// Obtener un contacto
app.get("/:id", function(req, res, next) {
  var idContacto = req.params.id;
  service
    .get(idContacto)
    .then(contacto => {
      res.send(contacto);
    })
    .catch(err => {
      var error = new Error("No se encontro el contacto");
      error.status = 404;
      next(error);
    });
});

// Agregar un contacto
app.post("/", function(req, res, next) {

  var contactoNuevo = {
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    email : req.body.email,
    ciudad : req.body.ciudad,
    provincia : req.body.provincia,
    pais : req.body.pais,
  };

  var instancia = new ContactoModel( contactoNuevo);
  service.save(instancia, true).then( contacto => {
    res.status(201).send(contacto); // Codigo HTTP 201 es creado
  }).catch(err => {
      next(new Error("No se pudo guardar el contacto"));
  })
});

// Actualizar un contacto
app.put("/:id", function(req, res, next) {

  var contactoActualizado = {
    _id : req.params.id,
    nombre : req.body.nombre,
    apellido : req.body.apellido,
    telefono : req.body.telefono,
    email : req.body.email,
    ciudad : req.body.ciudad,
    provincia : req.body.provincia,
    pais : req.body.pais,
  }

  var instancia = new ContactoModel(contactoActualizado);
  service
    .save(instancia, false)
    .then(contacto => {
      if (contacto) {
        res.status(204).send(contacto); // Codigo HTTP 204 es actualizado
      }
      else {
        next(new Error("No se pudo guardar el contacto"));
      }
    })
    .catch(err => {
      next(new Error("No se pudo guardar el contacto"));
    });  
});

// Borrar un contacto
app.delete("/:id", function(req, res, next) {
  var idContactoABorrar = req.params.id;
  service.delete(idContactoABorrar).then(obj => {
    res.status(204).send(); // Codigo HTTP 204 Sin contenido (no hay nada que responder, pero la operacion fue exitosa)
  }).catch(err => {
    var error = new Error("No se pudo borrar el contacto");
    error.status = 500;
    next(error);
  })

});

module.exports = app;
