var mongoose = require("mongoose");
require('mongoose-type-email');

// Definimos la estructura de los datos a guardar en Contacto
var ContactoSchema = mongoose.Schema({
    nombre: String,
    apellido: String,
    telefono: String,
    email: mongoose.SchemaTypes.Email,
    ciudad: String,
    provincia: String,
    pais: String,
});

// Creamos un model (es la representacion del contacto en nuestro sistema)
var ContactoModel = mongoose.model('Contacto', ContactoSchema);

module.exports = ContactoModel;